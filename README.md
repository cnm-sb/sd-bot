# 笋丁网页机器人
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本
## V3.0.0以及之后的版本，源码和教程请前往[http://gitee.com/cnm-sb/sd-web-bot](http://gitee.com/cnm-sb/sd-web-bot) 强烈建议使用新版本

## 程序介绍
- 网页机器人

- 前端Vue3，后端Golang


## 演示图

- 演示站：[http://bot.sunding.cn](http://bot.sunding.cn)

- 演示图
用户前台
![功能演示](other-images/demo-image.jpg)
![页面](other-images/user-demo.jpg)

管理员后台
![默认消息](other-images/admin-demo.png)
![自定义接口](other-images/admin-api-demo.png)
![用户管理](other-images/admin-user-demo.png)
![卡密充值](other-images/admin-rechargr-card-demo.png)

## 啦啦啦啦

-  程序所涉及的任何内容，包括但不限于文字，图片，图标，代码等等任何侵犯您权利的内容，请联系我们妥善处理！

-  企鹅交流群：826866498，大家加入群聊一起玩耍

    -  不想扫码？[点我加入](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=93nBg5lf-u3pR4bfSmMp4iiD4BjaxzSq&authKey=xuN0agwvBAckmy56wDcXr4N44HdTE4Qsdb5mOn8FDJzwBcpeaKf8LuedgQdBqd2X&noverify=0&group_code=826866498)
    -  <img src="other-images/826866498.jpg" alt="Q群826866498" width="300">


## 事先准备

- 一台服务器， **已安装宝塔面板**， **已安装宝塔面板** ，运行环境如下
    - 操作系统：Linux Centos7.6
    - Web服务器：Nginx 1.22.1
    - 数据库：Mysql 5.7.40
    - 数据库：Redis 7.0.11
    - 其他版本环境请自行测试

- 一个域名，域名先解析二条记录，方便后面使用
    - 前端页面：用户前台 demo.bot.sunding.cn（子目录admin为管理员后台 demo.bot.sunding.cn/admin）
    - 后端API：例如 api.demo.bot.sunding.cn

- 程序授权方式：
    - 一：加入QQ群，联系群主，购买授权
    - 二：添加微信www_cnm_sb，购买授权
    - 后端单域名授权20RMB/个，前端域名无限制，代理商88RMB，4折提卡，支持无限下级代理，三人回本，五人立享KFC，更多详情请咨询群主或添加微信www_cnm_sb

## 部署教程

### 🦀添加Mysql数据库
![添加Mysql数据库](deployment-tutorial/add-mysql.png)


### 🥝查看Redis数据库密码

![查看Redis数据库密码](deployment-tutorial/check-redis-password.png)


### 🍓部署前端用户前台和管理员后台

![添加前端站点](deployment-tutorial/create-website.png)
![上传前后端源码](deployment-tutorial/upload-code.png)


### 🍍部署后端API

    - 注意：端口为6633
    - 注意：端口为6633
    - 注意：端口为6633
    - 注意：端口为6633
    - 注意：端口为6633

![部署Go项目](deployment-tutorial/add-go.png)


### 🍊配置nginx规则

    # nginx反向代理代码
    location / {
        index index.html;
        try_files $uri $uri/ /index.html;
    }
    
    location /server/ {
        return 404;
    }

    location /admin/ {
        index index.html;
        try_files $uri $uri/ /admin/index.html;
    }
    
    location ^~ /sd {
    	proxy_pass http://请将此更换为Golang后端绑定的域名; # 请务必阅读下方注意事项
    	proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_http_version 1.1;
        proxy_set_header Connection ""; # 清除原始请求中的Connection头，Nginx会自动添加"keep-alive"
    }

注意事项
①proxy_pass和http之间一个空格此行末尾的分号不能丢，域名后面不能加“/”

②域名有解析才能保存，否则会报错 host not found

③注意后端域名的https和http协议：如果是http，则只能填http；如果是https且强制https，可以填http和https；如果后端是https且强制https，则必须是https

④参考图片的填写示例即可
![配置nginx规则](deployment-tutorial/set-nginx.png)


### 🥭执行安装
![初始化Mysql数据库](deployment-tutorial/mysql-install.png)
![连接Redis数据库](deployment-tutorial/redis-install.png)


### 🍋安装成功
![安装成功](deployment-tutorial/final-success.png)

管理员登录路径http(s)://域前端域名/admin

| 默认生成 | 用户名   | 密码     | 备注           |
|------|-------|--------|--------------|
| 管理员  | admin | 123456 | 登录成功之后请务必修改密码 |


### 🍉自此程序部署结束，感谢您的支持！
    搭建过程中有疑问请进入Q群咨询群友，有好的想法或建议意见也可告诉我们！

版本迭代

    - 后端：直接替换后端的main文件，storage文件夹和config.yaml文件不用管，替换main之后，前往go项目重启即可

    - 前端：前端包括用户前台和管理员后台，直接删除index.html文件，assets文件夹和admin目录，重新上传即可，nginx规则已经配置无需更改

## 自定义接口如何使用
当前自定义接口仅支持GET请求，仅支持json格式的响应，仅支持对象嵌套对象，支持对象嵌套数组，支持访问数组某一项的对象。支持图片，视频，文本。后续版本更新语音和音乐，POST请求

三个类型的参数不冲突，可以都填，至少填写一个

### 🥭精确匹配模式
不支持用户传参，接口链接直接填入，然后参数选择无需参数，把接口返回的json，使用占位符，单层{{.data}}，嵌套{{.data.url}}，数组{{.data.0}}，某个数组里的某个对象{{.data.0.title}}，这种格式，按需填写在回复内容/单图链接/视频链接的地方

### 🥭子串匹配模式
仅支持单个参数，用户发送的内容即为这个参数的值，链接里面使用占位符{{1}}传参，例如:http://bot.sunding.cn/api?text={{1}}，程序自动将{{1}}替换为参数值执行请求，对结果取值同无参数一样，使用{{.data}}

### 🥭前缀匹配模式
此模式支持1-6个参数，需要使用指令前缀来触发

一个参数时候，例如绘画功能，用户发送   绘画帮我画一只小猫（一个参数也可以使用绘画#帮我画一只小猫）  来触发，程序自动去除指令，链接:http://bot.sunding.cn/api?text={{1}}，去除指令绘画，剩余内容：帮我画一只小猫，即为参数一的值。

2个以及2个以上参数，链接里面这样填例如:http://bot.sunding.cn/api?text={{1}}&style={{2}}&color={{3}}&size={{4}}，需要几个填几个，
使用{{1}}，{{2}}，{{3}}，{{4}}，{{5}}，{{6}}来占位，依次递增，中间不能间隔，用户输入的时候，必须使用“#”来分割，例如两个参数时候：绘画#水墨画#帮我画一只可爱小猫，水墨画就会替换接口链接里面的{{1}}，帮我画一直可爱小猫，就会替换{{2}}

三个参数及以上同理，绘画#水墨风#帮我画一只小猫#红色  ，以此类推



## 🍈特别鸣谢

本项目使用了以下开源组件，并在此表示感谢：

- [Vue 3](https://github.com/vuejs/vue-next)
- [Vite](https://github.com/vitejs/vite)
- [Element Plus](https://github.com/element-plus/element-plus)
- [Pinia](https://github.com/vuejs/pinia)
- [Redis](https://github.com/redis/redis)
- [MySQL](https://github.com/mysql/mysql-server)

- [Golang](https://github.com/golang/go)
- [Gin](https://github.com/gin-gonic/gin)
- 等等，待完善

感谢以上组件的作者为我们提供优秀的开源软件！

Copyright (c) 2024 www.sunding.cn
